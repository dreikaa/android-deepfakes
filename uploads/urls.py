from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from uploads.core import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^uploads/simple/$', views.simple_upload, name='simple_upload'),

    url(r'^uploads/multiple/$', views.upload_multiple_file_and_start_processing,
        name='upload_multiple_file_and_start_processing'),

    url(r'^uploads/start/$', views.start_processing, name='start_processing'),

    url(r'^uploads/video/$', views.get_generated_video, name='get_generated_video'),
    url(r'^uploads/form/$', views.model_form_upload, name='model_form_upload'),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
