from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from django.utils.encoding import smart_str
from django.http import HttpResponse, HttpResponseNotFound

from uploads.core.models import Document
from uploads.core.forms import DocumentForm

from uploads.core.deepfakes.deepfakes import create_deepfakes_from_source_img_driving_video

import os
import json
import time
import magic
import threading
import hashlib

dir_path = '/root/generated_files/generated/'


def home(request):
    documents = Document.objects.all()
    return render(request, 'core/home.html', {'documents': documents})


@csrf_exempt
def get_generated_video(request):
    print("-------------------------------------------------------------")
    print("get_generated_video")
    if request.method == 'POST':
        file_name = request.POST['video_file_name']

        file_name = file_name.replace('.mp4', '_finished.mp4')

        if not os.path.exists(dir_path + file_name):
            return HttpResponseNotFound("File not ready yet.")

        print()
        # while not os.path.isfile(dir_path + file_name):
        #     end = time.time()
        #     if end - start > 256:
        #         return HttpResponseNotFound("The process is out of time")

        mime = magic.Magic(mime=True)
        mime_type = mime.from_file(dir_path + file_name)

        print("Opening the stream for response")
        with open(dir_path + file_name, 'rb') as f:
            data = f.read()

        response = HttpResponse(data, content_type=mime_type)
        response['Content-Disposition'] = 'attachment; filename=' + file_name
        print("Response is successfully sent")
        print("-------------------------------------------------------------")
        return response
    else:
        print("The request get_generated_video not POST type")
        print("-------------------------------------------------------------")
        return HttpResponseNotFound("Not post")


@csrf_exempt
def start_processing(request):
    print("-------------------------------------------------------------")
    print("start_processing")
    if request.method == 'POST':
        dir = '/root/android-deepfakes/media/'

        if not "source_image_file_name" in request.POST \
                or not "driving_video_file_name" in request.POST:
            print("The request doesn't have required fields")

        else:
            source_image = dir + request.POST["source_image_file_name"]
            driving_video = dir + request.POST["driving_video_file_name"]

            create_deepfakes_from_source_img_driving_video(source_image, driving_video)
            print("The request start_processing completed successfully")
            print("-------------------------------------------------------------")

            os.remove(dir + request.POST["source_image_file_name"])
            os.remove(dir + request.POST["driving_video_file_name"])
            os.remove(dir + request.POST["driving_video_file_name"].replace('.mp4', '_retrieved_audio.mp3'))

            return HttpResponse()

    else:
        print("The request get_generated_video not POST type")
    print("-------------------------------------------------------------")
    return HttpResponseNotFound("Not working")


@csrf_exempt
def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)

        print(filename + " is received")

        uploaded_file_url = fs.url(filename)
        return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url
        })

    return render(request, 'core/simple_upload.html')


# @csrf_exempt
# def check_if_files_are_uploaded(request):
#     print("-------------------------------------------------------------")
#     print("upload_multiple_file")
#     if request.method == 'POST':
#         dir = '/root/android-deepfakes/media/'
#
#         if not "source_image_file_name" in request.POST \
#                 or not "driving_video_file_name" in request.POST:
#             print("The request doesn't have required fields")
#
#         else:
#             source_image = dir + request.POST["source_image_file_name"]
#             driving_video = dir + request.POST["driving_video_file_name"]
#
#             source_image_hash = request.POST["source_image_file_name_hash"]
#             driving_video_hash = request.POST["driving_video_file_name_hash"]
#
#             if os.path.isfile()
#             hashlib.md5(source_image).digest()
#             hashlib.md5(driving_video).digest()


# def start_processing_in_daemon(source_image, driving_video):
#     create_deepfakes_from_source_img_driving_video(source_image, driving_video)
#     print("The request start_processing completed successfully")
#     print("-------------------------------------------------------------")
#
#     try:
#         os.remove(source_image)
#         os.remove(driving_video)
#         os.remove(driving_video.replace('.mp4', '_retrieved_audio.mp3'))
#     except OSError:
#         pass


@csrf_exempt
def upload_multiple_file_and_start_processing(request):
    print("-------------------------------------------------------------")
    print("upload_multiple_file")
    if request.method == 'POST':
        myfiles = request.FILES.getlist('myfile')

        source_image = myfiles[0].name
        driving_video = myfiles[1].name
        source_dir = '/root/android-deepfakes/media/'

        source_image = source_dir + source_image
        driving_video = source_dir + driving_video

        res = '/root/generated_files/generated' + '/' + driving_video.split("/")[-1].replace('.mp4',
                                                                                             '_finished.mp4')
        if os.path.exists(res) and os.path.exists(source_image) or \
                os.path.exists(source_image) and os.path.exists(driving_video):
            print("Generated file already exists; Skipping saving part")
        else:
            print("source_image: " + source_image)
            print("driving_video: " + driving_video)

            if os.path.isfile(source_image) or os.path.isfile(driving_video):
                print("Uploading files already exists; Skipping saving part")
            else:
                for myfile in myfiles:
                    fs = FileSystemStorage()
                    filename = fs.save(myfile.name, myfile)
                    print(filename + " is received")

            print("All files from the request are successfully received")
            print("Starting deepfakes generation")

            if not os.path.isfile(source_image) or not os.path.isfile(driving_video):
                print("Uploaded files doesn't exist. Some error occurred during upload and saving")
                print("-------------------------------------------------------------")
                return HttpResponseNotFound(
                    "Uploaded files doesn't exist. Some error occurred during upload and saving")

            # d = threading.Thread(name='daemon', target=start_processing_in_daemon, args=(source_image, driving_video,))
            # d.setDaemon(True)
            #
            # d.start()

            create_deepfakes_from_source_img_driving_video(source_image, driving_video)
            print("The request start_processing completed successfully")
            print("-------------------------------------------------------------")

            # try:
            # os.remove(source_image)
            # os.remove(driving_video)
            # os.remove(driving_video.replace('.mp4', '_retrieved_audio.mp3'))
            # except OSError:
            #     pass

        return HttpResponse("The request upload_multiple_file_and_start_processing completed successfully")

    else:
        print("The request upload_multiple_file not POST type")
    return HttpResponseNotFound("")


@csrf_exempt
def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = DocumentForm()
    return render(request, 'core/model_form_upload.html', {
        'form': form
    })
