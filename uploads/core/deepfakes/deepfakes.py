import os
import cv2
import imageio
import numpy as np
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from uploads.core.deepfakes.demo import load_checkpoints
from skimage.transform import resize
from IPython.display import HTML
from uploads.core.deepfakes.demo import make_animation
from skimage import img_as_ubyte

import subprocess
import warnings

warnings.filterwarnings("ignore")

dir_path = '/root/generated_files/generated/'
audio_dir_path = '/root/generated_files/audio/'


# def convert_to_30_fps(video_file_path):
#     new_video_file_path = video_file_path.replace('.mp4', '_reduced_fps_to_30.mp4')
#     #     os.system("ffmpeg -i {0} -r 30 -y {1}".format(video_file_path, new_video_file_path))
#     os.system("ffmpeg -i {0} -filter:v fps=fps=30 {1}".format(video_file_path, new_video_file_path))
#
#     return new_video_file_path


def merge_audio_and_video_files(video_file_path, audio_file_path):
    print("merge video and audio")
    command = "ffmpeg -i " + video_file_path + " -i " + audio_file_path + " audio.wav -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 " + video_file_path

    subprocess.call(command, shell=True)


def video_fps(driving_video_path):
    video = cv2.VideoCapture(driving_video_path)

    # Find OpenCV version
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

    if int(major_ver) < 3:
        fps = video.get(cv2.cv.CV_CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
    else:
        fps = video.get(cv2.CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
    return fps


def get_audio_from_video(video_file_path):
    audio_output_path = video_file_path.replace('.mp4', '_retrieved_audio.mp3')
    print("-------------------------------------------------------------")
    print("get_audio_from_video")
    print("video_file_path: " + video_file_path)
    print("audio_output_path: " + audio_output_path)
    subprocess.call(['ffmpeg', '-y', '-i', video_file_path, '-f', 'mp3', '-ab', '192000', '-vn', audio_output_path])
    # os.system("ffmpeg -y -i {0} -f mp3 -ab 192000 -vn {1}".format(video_file_path, audio_output_path))
    print("-------------------------------------------------------------")
    return audio_output_path


def merge_audio_in_video(video_file_path, audio_file_path):
    video_output_path = video_file_path.replace('.mp4', '_merged_with_audio.mp4')
    print("-------------------------------------------------------------")
    print("merge_audio_in_video")
    print("video_file_path: " + video_file_path)
    print("audio_file_path: " + audio_file_path)
    print("video_output_path: " + video_output_path)
    subprocess.call(
        ['ffmpeg', '-y', '-i', video_file_path, '-i', audio_file_path, '-shortest', '-c:v', 'copy', '-c:a', 'aac',
         '-b:a', '256k', video_output_path])
    # os.system(
    #     "ffmpeg -y -i {0} -i {1} -shortest -c:v copy -c:a aac -b:a 256k {2}".format(video_file_path, audio_file_path,
    #                                                                                 video_output_path))
    print("-------------------------------------------------------------")
    return video_output_path


def display(driving_video_path, source=None, driving=None, generated=None, driving_length=None):
    fig = plt.figure(figsize=(8 + 4 * (generated is not None), 6))

    if driving is not None:
        driving_length = len(driving)

    ims = []
    for i in range(driving_length):
        cols = []
        if source is not None:
            cols = [source]
        if driving is not None:
            cols.append(driving[i])
        if generated is not None:
            cols.append(generated[i])
        im = plt.imshow(np.concatenate(cols, axis=1), animated=True)
        plt.axis('off')
        ims.append([im])
    interval = round(1000 / video_fps(driving_video_path))
    ani = animation.ArtistAnimation(fig, ims, interval=interval, repeat_delay=1000)

    # Set up formatting for the movie files
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=30, metadata=dict(artist=fig), bitrate=1800)

    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    resulting_path = dir_path + '/' + driving_video_path.split("/")[-1]

    print("Resulting file name: " + driving_video_path.split("/")[-1])
    ani.save(resulting_path, writer=writer)

    plt.close()

    return resulting_path


def create_deepfakes_from_source_img_driving_video(source_image_path, driving_video_path):
    audio_output_path = get_audio_from_video(driving_video_path)

    source_image = imageio.imread(source_image_path)
    driving_video = imageio.mimread(driving_video_path, memtest=False)

    # Resize image and video to 256x256
    source_image = resize(source_image, (256, 256))[..., :3]
    driving_video = [resize(frame, (256, 256))[..., :3] for frame in driving_video]

    # load tar for generating
    generator, kp_detector = load_checkpoints(
        config_path='/root/android-deepfakes/uploads/core/deepfakes/config/vox-256.yaml',
        checkpoint_path='/root/android-deepfakes/uploads/core/deepfakes/vox-cpk.pth.tar')

    # creation of predictions and saving into mp4 file
    predictions = make_animation(source_image, driving_video, generator, kp_detector, relative=True)

    # imageio.mimsave(dir_path + 'generated.mp4', [img_as_ubyte(frame) for frame in predictions])
    print("Generation is over. Check the file by path: " + dir_path)
    resulting_video_path = display(driving_video_path, driving_length=len(driving_video), generated=predictions)

    print("Getting audio from initial path")
    merge_audio_in_video(resulting_video_path, audio_output_path)

    resulting_video_path_with_audio = resulting_video_path.replace('.mp4', '_merged_with_audio.mp4')
    print("Resulting file name: " + resulting_video_path.split("/")[-1])
    print("Resulting file name after replace to _merged_with_audio: " + resulting_video_path_with_audio.split("/")[-1])

    if os.path.isfile(resulting_video_path_with_audio):
        # os.remove(resulting_video_path)
        os.rename(resulting_video_path_with_audio,
                  resulting_video_path_with_audio.replace('_merged_with_audio.mp4', '_finished.mp4'))
        print("returning file will have sound")
    else:
        os.rename(resulting_video_path, resulting_video_path.replace('.mp4', '_finished.mp4'))
        print("returning file will not have sound")
